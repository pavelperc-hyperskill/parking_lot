package parkingLot

import org.hyperskill.hstest.v5.stage.BaseStageTest
import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.test_utils.KotlinTest
import org.hyperskill.test_utils.testCase
import kotlin.reflect.jvm.javaMethod


class Task1Test : KotlinTest<Unit>(::main) {
    
    override fun generate() = listOf(testCase(Unit, ""))
    
    override fun check(reply: String, clue: Unit): CheckResult {
        val text = """
            White car has parked.
            Yellow car left the parking lot.
            Green car just parked here.
        """.trimIndent()
        
        if (reply.trim() != text.trim()) {
            return CheckResult.FALSE("You printed the wrong text! See the example.")
        } else {
            return CheckResult.TRUE
        }
        
    }
}

