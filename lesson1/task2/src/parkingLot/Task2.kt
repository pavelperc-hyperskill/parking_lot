package parkingLot

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val command = scanner.next()
    when (command) {
        "park" -> {
            val reg = scanner.next()
            val color = scanner.next()
            println("$color car parked on the spot 2.")
        }
        "leave" -> {
            val number = scanner.nextInt()
            if (number == 2) {
                println("There is no car in the spot $number.")
            } else {
                println("Spot $number is free.")
            }
        }
    }
}