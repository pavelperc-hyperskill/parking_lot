package parkingLot

import org.hyperskill.hstest.v5.stage.BaseStageTest
import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.test_utils.*
import kotlin.reflect.jvm.javaMethod


class Task2Test : KotlinTest<OutputClue>(::main) {
    
    override fun generate() = listOf(
            outputCase("park KA-01-HH-1234 White",
                    "White car parked on the spot 2.", hint = "See example 1."),
            outputCase("leave 1",
                    "Spot 1 is free.", hint = "See example 2."),
            outputCase("leave 2",
                    "There is no car in the spot 2.", hint = "See example 3."),
            outputCase("park KA-01-HH-1234 Red",
                    "Red car parked on the spot 2.", true, hint = "Try to test another colors."),
            outputCase("park 1ABC234 Blue",
                    "Blue car parked on the spot 2.", true, hint = "Try to test another registration numbers.")
    )
    
    
    override fun check(reply: String, clue: OutputClue): CheckResult {
        
        return clue.compareLines(reply)
    }
}

