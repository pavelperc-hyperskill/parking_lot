package parkingLot

import java.util.*


data class Spot(
        val car: String,
        val color: String
)

class Parking(val capacity: Int) {
    
    private val spots = Array<Spot?>(capacity) { null }
    
    fun park(car: String, color: String) {
        val emptyIdx = spots.indexOfFirst { it == null }
        
        if (emptyIdx == -1) {
            println("Sorry, parking lot is full.")
        } else {
            spots[emptyIdx] = Spot(car, color)
            println("$color car parked on the spot ${emptyIdx + 1}.")
        }
    }
    
    fun leave(position: Int) {
        if (spots[position - 1] == null) {
            println("There is no car in the spot $position.")
        } else {
            spots[position - 1] = null
            println("Spot $position is free.")
        }
    }
}


fun main() {
    val scanner = Scanner(System.`in`)
    
    val parking = Parking(20)
    
    wh@ while (true) {
        val command = scanner.next()
        when (command) {
            "park" -> {
                val car = scanner.next()
                val color = scanner.next()
                parking.park(car, color)
            }
            "leave" -> {
                val position = scanner.nextInt()
                parking.leave(position)
            }
            "exit" -> {
                break@wh
            }
            else -> println("Unknown command: $command")
        }
        
    }
}