package parkingLot

import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.hstest.v5.testcase.TestCase
import org.hyperskill.test_utils.KotlinTest
import org.hyperskill.test_utils.OutputClue
import org.hyperskill.test_utils.outputCase
import org.hyperskill.test_utils.trimAllIndents


class Task3Test : KotlinTest<OutputClue>(::main) {
    
    override fun generate(): List<TestCase<OutputClue>> {
        // 20 cars
        val stripedCars = List(10) { i ->
            listOf("park KA-$i-HH-9999 White", "park KA-$i-HH-3672 Green")
        }
                .flatten()
                .joinToString("\n")
        
        val stripedAns = List(10) { i ->
            listOf("White car parked on the spot ${2 * i + 1}.",
                    "Green car parked on the spot ${2 * i + 2}.")
        }
                .flatten()
                .joinToString("\n")
        
        
        return listOf(
                outputCase(
                        """$stripedCars
                            park Rs-P-N-21 Red
                            leave 1
                            park Rs-P-N-21 Red
                            exit
                        """.trimAllIndents(),
                        """
                            $stripedAns
                            Sorry, parking lot is full.
                            Spot 1 is free.
                            Red car parked on the spot 1.
                        """.trimAllIndents(),
                        hint = "See example 1."),
                outputCase(
                        """
                            $stripedCars
                            park Rs-P-N-21 Red
                            park ABC Green
                            leave 5
                            leave 1
                            leave 20
                            park Rs-P-N-21 White
                            park Rs-P-N-22 Blue
                            park Rs-P-N-23 Red
                            park A B
                            exit
                        """.trimAllIndents(),
                        """
                            $stripedAns
                            Sorry, parking lot is full.
                            Sorry, parking lot is full.
                            Spot 5 is free.
                            Spot 1 is free.
                            Spot 20 is free.
                            White car parked on the spot 1.
                            Blue car parked on the spot 5.
                            Red car parked on the spot 20.
                            Sorry, parking lot is full.
                        """.trimAllIndents(),
                        isPrivate = true,
                        hint = "Spots should be assigned in ascending order.")
                )
    }
    
    
    override fun check(reply: String, clue: OutputClue): CheckResult {
        
        return clue.compareLines(reply)
    }
}

