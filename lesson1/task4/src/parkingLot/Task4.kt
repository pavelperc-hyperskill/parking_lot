package parkingLot

import java.util.*


data class Spot(
        val car: String,
        val color: String
)

class Parking(capacity: Int) {
    
    private val spots = Array<Spot?>(capacity) { null }
    
    fun park(car: String, color: String) {
        val emptyIdx = spots.indexOfFirst { it == null }
        
        if (emptyIdx == -1) {
            println("Sorry, parking lot is full.")
        } else {
            spots[emptyIdx] = Spot(car, color)
            println("$color car parked on the spot ${emptyIdx + 1}.")
        }
    }
    
    fun leave(position: Int) {
        if (spots[position - 1] == null) {
            println("There is no car in the spot $position.")
        } else {
            spots[position - 1] = null
            println("Spot $position is free.")
        }
    }
    
    fun status() {
        val filtered = spots.withIndex()
                .filter { (_, spot) -> spot != null }
                .map { (i, spot) -> i to spot!! }
        
        if (filtered.isEmpty()) {
            println("Parking lot is empty.")
        } else {
            filtered.forEach { (i, spot) ->
                println("${i + 1} ${spot.car} ${spot.color}")
            }
        }
    }
}


fun main() {
    val scanner = Scanner(System.`in`)
    
    var parking: Parking? = null
    
    fun ifHaveParking(op: (parking: Parking) -> Unit) {
        parking?.also(op)
                ?: println("Sorry, parking lot is not created.")
    }
    
    wh@ while (true) {
        val command = scanner.next()
        when (command) {
            "create" -> {
                val capacity = scanner.nextInt()
                parking = Parking(capacity)
                println("Created a parking lot with $capacity spots.")
            }
            "park" -> {
                val car = scanner.next()
                val color = scanner.next()
                ifHaveParking { p ->
                    p.park(car, color)
                }
                
            }
            "leave" -> {
                val position = scanner.nextInt()
                ifHaveParking { p ->
                    p.leave(position)
                }
            }
            "status" -> {
                ifHaveParking { p ->
                    p.status()
                }
            }
            "exit" -> {
                break@wh
            }
            else -> println("Unknown command: $command")
        }
        
    }
}