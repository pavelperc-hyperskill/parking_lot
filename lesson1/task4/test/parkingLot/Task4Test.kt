package parkingLot

import org.hyperskill.hstest.v5.stage.BaseStageTest
import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.test_utils.*
import kotlin.reflect.jvm.javaMethod


class Task4Test : KotlinTest<OutputClue>(::main) {
    
    override fun generate() = listOf(
            outputCase(
                    """
                        park KA-01-HH-9999 White
                        create 3
                        status
                        park KA-01-HH-9999 White
                        park KA-01-HH-3672 Green
                        park Rs-P-N-21 Red
                        leave 2
                        status
                        exit
                    """.trimAllIndents(),
                    """
                        Sorry, parking lot is not created.
                        Created a parking lot with 3 spots.
                        Parking lot is empty.
                        White car parked on the spot 1.
                        Green car parked on the spot 2.
                        Red car parked on the spot 3.
                        Spot 2 is free.
                        1 KA-01-HH-9999 White
                        3 Rs-P-N-21 Red
            """.trimAllIndents(),
                    hint = "See example 1."),
            outputCase(
                    """
                        park KA-01-HH-9999 White
                        leave 1
                        status
                        exit
                    """.trimAllIndents(),
                    """
                        Sorry, parking lot is not created.
                        Sorry, parking lot is not created.
                        Sorry, parking lot is not created.
            """.trimAllIndents(),
                    true,
                    hint = "Check commands until the parking is created."),
            outputCase(
                    """
                        create 3
                        park KA-01-HH-9999 White
                        park KA-01-HH-9998 Red
                        status
                        create 1
                        status
                        park KA-01-HH-9999 Black
                        status
                        park KA-01-HH-9998 Black
                        exit
                    """.trimAllIndents(),
                    """
                        Created a parking lot with 3 spots.
                        White car parked on the spot 1.
                        Red car parked on the spot 2.
                        1 KA-01-HH-9999 White
                        2 KA-01-HH-9998 Red
                        Created a parking lot with 1 spots.
                        Parking lot is empty.
                        Black car parked on the spot 1.
                        1 KA-01-HH-9999 Black
                        Sorry, parking lot is full.
            """.trimAllIndents(),
                    true,
                    hint = "Try to recreate the parking.")
            
            
    )
    
    
    override fun check(reply: String, clue: OutputClue): CheckResult {
        
        return clue.compareLines(reply)
    }
}

