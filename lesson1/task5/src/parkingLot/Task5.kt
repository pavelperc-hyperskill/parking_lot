package parkingLot

import java.util.*


data class Spot(
        val car: String,
        val color: String
)

class Parking(capacity: Int) {
    
    private val spots = Array<Spot?>(capacity) { null }
    
    fun park(car: String, color: String) {
        val emptyIdx = spots.indexOfFirst { it == null }
        
        if (emptyIdx == -1) {
            println("Sorry, parking lot is full.")
        } else {
            if (car in spots.map { it?.car }) {
                throw IllegalStateException("Bad test! repeating reg num: $car")
            }
            
            spots[emptyIdx] = Spot(car, color)
            println("$color car parked on the spot ${emptyIdx + 1}.")
        }
    }
    
    fun leave(position: Int) {
        if (spots[position - 1] == null) {
            println("There is no car in the spot $position.")
        } else {
            spots[position - 1] = null
            println("Spot $position is free.")
        }
    }
    
    fun status() {
        val filtered = spots.withIndex()
                .filter { (_, spot) -> spot != null }
                .map { (i, spot) -> i to spot!! }
        
        if (filtered.isEmpty()) {
            println("Parking lot is empty.")
        } else {
            filtered.forEach { (i, spot) ->
                println("${i + 1} ${spot.car} ${spot.color}")
            }
        }
    }
    
    fun regByColor(color: String) {
        val requiredColor = color.toLowerCase()
        val filtered = spots
                .filterNotNull()
                .filter {
                    it.color.toLowerCase() == requiredColor
                }
        
        if (filtered.isEmpty()) {
            println("No cars with color $color were found.")
            return
        }
        println(filtered.joinToString(", ") { it.car })
    }
    
    fun spotsByColor(color: String) {
        val requiredColor = color.toLowerCase()
        val filtered = spots.withIndex()
                .filter { (_, spot) ->
                    spot?.color?.toLowerCase() == requiredColor
                }
                .map { it.index + 1}
        
        if (filtered.isEmpty()) {
            println("No cars with color $color were found.")
            return
        }
        println(filtered.joinToString(", "))
    }
    
    fun spotsByReg(reg: String) {
        val filtered = spots.withIndex()
                .filter { (_, spot) ->
                    spot?.car == reg
                }
                .map { it.index + 1 }
        
        if (filtered.isEmpty()) {
            println("No cars with registration number $reg were found.")
            return
        }
        println(filtered.joinToString(", "))
    }
}


fun main() {
    val scanner = Scanner(System.`in`)
    
    var parking: Parking? = null
    
    fun ifHaveParking(op: (parking: Parking) -> Unit) {
        parking?.also(op)
                ?: println("Sorry, parking lot is not created.")
    }
    
    wh@ while (true) {
        val command = scanner.next()
        when (command) {
            "create" -> {
                val capacity = scanner.nextInt()
                if (parking != null) {
                    println("Parking lot is already created.")
                } else {
                    parking = Parking(capacity)
                    println("Created a parking lot with $capacity spots.")
                }
            }
            "park" -> {
                val car = scanner.next()
                val color = scanner.next()
                ifHaveParking { p ->
                    p.park(car, color)
                }
                
            }
            "leave" -> {
                val position = scanner.nextInt()
                ifHaveParking { p ->
                    p.leave(position)
                }
            }
            "status" -> {
                ifHaveParking { p ->
                    p.status()
                }
            }
            "reg_by_color" -> {
                val color = scanner.next()
                ifHaveParking { p ->
                    p.regByColor(color)
                }
            }
            "spot_by_color" -> {
                val color = scanner.next()
                ifHaveParking { p ->
                    p.spotsByColor(color)
                }
            }
            "spot_by_reg" -> {
                val reg = scanner.next()
                ifHaveParking { p ->
                    p.spotsByReg(reg)
                }
            }
            "exit" -> {
                break@wh
            }
            else -> println("Unknown command: $command")
        }
        
    }
}