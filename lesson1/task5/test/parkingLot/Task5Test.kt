package parkingLot

import org.hyperskill.hstest.v5.stage.BaseStageTest
import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.test_utils.*
import kotlin.reflect.jvm.javaMethod


class Task5Test : KotlinTest<OutputClue>(::main) {
    
    override fun generate() = listOf(
            outputCase(
                    """
                        spot_by_color yellow
                        create 4
                        park KA-01-HH-9999 White
                        park KA-01-HH-3672 White
                        park Rs-P-N-21 Red
                        park Rs-P-N-22 Red
                        reg_by_color GREEN
                        reg_by_color WHITE
                        spot_by_color GREEN
                        spot_by_color red
                        spot_by_reg ABC
                        spot_by_reg KA-01-HH-3672
                        spot_by_reg Rs-P-N-21
                        exit
                    """.trimAllIndents(),
                    """
                        Sorry, parking lot is not created.
                        Created a parking lot with 4 spots.
                        White car parked on the spot 1.
                        White car parked on the spot 2.
                        Red car parked on the spot 3.
                        Red car parked on the spot 4.
                        No cars with color GREEN were found.
                        KA-01-HH-9999, KA-01-HH-3672
                        No cars with color GREEN were found.
                        3, 4
                        No cars with registration number ABC were found.
                        2
                        3
            """.trimAllIndents(),
                    hint = "See example 1."
            ),
            outputCase(
                    """
                        spot_by_color yellow
                        reg_by_color yellow
                        spot_by_reg yellow
                        exit
                    """.trimAllIndents(),
                    """
                        Sorry, parking lot is not created.
                        Sorry, parking lot is not created.
                        Sorry, parking lot is not created.
            """.trimAllIndents(),
                    isPrivate = true,
                    hint = "Check commands until the parking is created."
            ),
            outputCase(
                    """
                        create 5
                        park A AA
                        park B BB
                        park C BB
                        park D AA
                        park E AA
                        leave 4
                        reg_by_color aa
                        reg_by_color bb
                        reg_by_color zz
                        spot_by_color aa
                        spot_by_color bb
                        spot_by_color zz
                        spot_by_reg A
                        spot_by_reg B
                        spot_by_reg Z
                        exit
                    """.trimAllIndents(),
                    """
                        Created a parking lot with 5 spots.
                        AA car parked on the spot 1.
                        BB car parked on the spot 2.
                        BB car parked on the spot 3.
                        AA car parked on the spot 4.
                        AA car parked on the spot 5.
                        Spot 4 is free.
                        A, E
                        B, C
                        No cars with color zz were found.
                        1, 5
                        2, 3
                        No cars with color zz were found.
                        1
                        2
                        No cars with registration number Z were found.
            """.trimAllIndents(),
              isPrivate = true,
                    hint = "Check the case, when the parking is not full."
            )
    
    )
    
    override fun check(reply: String, clue: OutputClue): CheckResult {
        return clue.compareLines(reply)
    }
}

