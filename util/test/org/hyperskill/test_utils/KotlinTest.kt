package org.hyperskill.test_utils

import org.hyperskill.hstest.v5.stage.BaseStageTest
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.javaMethod

abstract class KotlinTest<T>(main: KFunction<*>)
    : BaseStageTest<T>(main.javaMethod!!.declaringClass) {
}