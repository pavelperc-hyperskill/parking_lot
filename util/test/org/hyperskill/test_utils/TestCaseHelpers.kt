package org.hyperskill.test_utils

import org.hyperskill.hstest.v5.testcase.CheckResult
import org.hyperskill.hstest.v5.testcase.TestCase


/** Default testCase. */
fun <T> testCase(attach: T, input: String) = TestCase<T>().apply {
    setInput(input)
    setAttach(attach)
}

open class InputClue(
        val input: String,
        /** Do not show correct output and input. */
        val isPrivate: Boolean = false,
        /** Hint will be printed even for private tests. */
        val hint: String? = null
) {
    
    /** Ciphers [message] or adds a [hint] to the error if it is not null. */
    fun toFailure(message: String): CheckResult {
        if (isPrivate) {
            // Ciphered
            return CheckResult.FALSE("Incorrect output. This is a private test. " + (hint ?: ""))
        } else {
            return CheckResult.FALSE("$message ${hint ?: ""}")
        }
    }
}

fun inputCase(
        input: String,
        isPrivate: Boolean = false,
        hint: String? = null
) = testCase(InputClue(input, isPrivate, hint), input)

class OutputClue(input: String, val output: String, isPrivate: Boolean, hint: String?) 
    : InputClue(input, isPrivate, hint) {
    
    fun compareLines(reply: String) : CheckResult {
        val hisLines = reply.trim().lines()
        val myLines = output.trim().lines()
        
        myLines.zip(hisLines).withIndex().forEach { (i, pair) ->
            val (my, his) = pair
            if (my != his) {
                return toFailure("Error in line ${i + 1}: \"$his\" instead of \"$my\".")
            }
        }
        // if all common lines are correct, but sizes are different.
        if (hisLines.size != myLines.size) {
            return toFailure("Your output contains $hisLines.size lines instead of ${myLines.size}.")
        }
        return CheckResult.TRUE
    }
}

fun outputCase(
        input: String,
        output: String,
        isPrivate: Boolean = false,
        hint: String? = null
) = testCase(OutputClue(input, output, isPrivate, hint), input)

/** Trim Starts of all lines and trim empty lines. */
fun String.trimAllIndents() = this.lines()
        .map { it.trimStart() }
        .dropWhile { it.isBlank() }
        .dropLastWhile { it.isBlank() }
        .joinToString("\n")




